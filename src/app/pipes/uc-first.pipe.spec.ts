import { UcFirstPipe } from './uc-first.pipe';

describe('UcFirstPipe', () => {
  describe('when pipe is created', () => {
    let pipe: UcFirstPipe;

    beforeAll(() => {
      pipe = new UcFirstPipe();
    });

    it.each([
      {input: undefined, text: 'undefined'},
      {input: null, text: 'null'},
      {input: '', text: 'empty string'},
    ])('should transform $text to empty string', ({input, text}) => {
      expect(pipe.transform(input)).toBe('');
    });

    it.each([
      {input:' ', expected:' '},
      {input:' text', expected:' text'},
      {input:'text', expected:'Text'},
      {input:'Text', expected:'Text'},
      {input:'a', expected:'A'},
      {input:'1a', expected:'1a'},
      {input:'TEXT ', expected:'TEXT '},
    ])('should transform "$input" to "$expected"', ({input, expected}) => {
      expect(pipe.transform(input)).toBe(expected);
    });
  });
});
