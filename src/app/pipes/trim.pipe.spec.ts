import { TrimPipe } from './trim.pipe';

describe('TrimPipe', () => {
  describe('when pipe is created', () => {
    let pipe: TrimPipe;

    beforeAll(() => {
      pipe = new TrimPipe();
    });

    it.each([
      {input: undefined, text: 'undefined'},
      {input: null, text: 'null'},
      {input: '', text: 'empty string'},
      {input: '\n   \n', text: '"\\n   \\n"'},
    ])('should transform $text to empty string', ({input, text}) => {
      expect(pipe.transform(input)).toBe('');
    });

    it.each([
      {input: '  text', text: 'text', expectedText: 'text', expected: 'text'},
      {input: 'text  ', text: 'text', expectedText: 'text', expected: 'text'},
      {input: 'text text', text: 'text text', expectedText: 'text text', expected: 'text text'},
      {input: '   text text  ', text: 'text text', expectedText: 'text text', expected: 'text text'},
      {input: '   text \n text  ', text: 'text \\n text', expectedText: 'text \\n text', expected: 'text \n text'},
      {
        input: '\n   text \n text  \n',
        text: '\\n   text \\n text  \\n',
        expectedText: 'text \\n text',
        expected: 'text \n text'
      },
    ])('should transform "$text" to "$expectedText"', ({input, expected, expectedText}) => {
      expect(pipe.transform(input)).toBe(expected);
    });
  });
});
