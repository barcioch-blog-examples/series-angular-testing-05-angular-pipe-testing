import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appTrim',
})
export class TrimPipe implements PipeTransform {

  transform(input: string | null | undefined): string {
    if (input == null || input.length === 0) {
      return '';
    }

    return input.trim();
  }
}
