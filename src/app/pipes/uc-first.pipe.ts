import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appUcFirst',
})
export class UcFirstPipe implements PipeTransform {

  transform(input: string | null | undefined): string {
    if (input == null || input.length === 0) {
      return '';
    }

    return input[0].toUpperCase() + input.substring(1, input.length);
  }
}
