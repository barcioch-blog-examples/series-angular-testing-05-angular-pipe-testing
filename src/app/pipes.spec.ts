import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TrimPipe } from './pipes/trim.pipe';
import { UcFirstPipe } from './pipes/uc-first.pipe';
import { Component, DebugElement, Input } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('Test Pipes', () => {
  let fixture: ComponentFixture<AppWrapperComponent>;
  let component: AppWrapperComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TrimPipe,
        UcFirstPipe,
        AppTestComponent,
        AppWrapperComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppWrapperComponent);
    component = fixture.componentInstance;
  });

  describe.each([
    {input: '', inputText: '', expected: '', expectedText: ''},
    {input: 'article', inputText: 'article', expected: 'Article', expectedText: 'Article'},
    {input: '  article  ', inputText: '  article  ', expected: 'Article', expectedText: 'Article'},
    {input: '  new article  ', inputText: '  new article  ', expected: 'New article', expectedText: 'New article'},
    {
      input: '  \n new article \n  ',
      inputText: '  \\n new article \\n  ',
      expected: 'New article',
      expectedText: 'New article'
    },
    {
      input: '  \n new \n article \n  ',
      inputText: '  \\n new \\n article \\n  ',
      expected: 'New \n article',
      expectedText: 'New \\n article'
    },
  ])('when "$inputText" is passed', ({input, inputText, expected, expectedText}) => {
    beforeEach(() => {
      component.title = input;
      fixture.detectChanges();
    });

    it(`should display "${ expectedText }"`, () => {
      expect(title().nativeElement.textContent).toBe(expected);
    });
  });

  const title = (): DebugElement => {
    return fixture.debugElement.query(By.css('.title'));
  }
});


@Component({
  selector: 'app-test',
  template: '<span class="title">{{ title | appTrim | appUcFirst }}</span>',
})
export class AppTestComponent {
  @Input() title: string | null | undefined;
}

@Component({
  selector: 'app-wrapper',
  template: '<app-test [title]="title"></app-test>',
})
export class AppWrapperComponent {
  title: string | null | undefined;
}
